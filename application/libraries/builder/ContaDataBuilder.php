<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/CI_Object.php';

class ContaDataBuilder extends CI_Object{
	protected $contas = [
	[
			'parceiro' => 'Magalu', 
			'descricao' => 'Notebook', 
			'valor' => '2000', 
			'mes' => 1, 
			'ano' => 2021, 
			'tipo' => 'pagar'
	],
	[
			'parceiro' => 'Casas Bahia', 
			'descricao' => 'Celular', 
			'valor' => '1500', 
			'mes' => 1, 
			'ano' => 2021, 
			'tipo' => 'pagar'
	],
	[
			'parceiro' => 'Americanas', 
			'descricao' => '�gua', 
			'valor' => '100', 
			'mes' => 1, 
			'ano' => 2021, 
			'tipo' => 'pagar'
	],
	[
			'parceiro' => 'Bandeirantes', 
			'descricao' => 'Energia', 
			'valor' => '700', 
			'mes' => 1, 
			'ano' => 2021, 
			'tipo' => 'receber'
	],
	[
			'parceiro' => 'Magazine', 
			'descricao' => 'Notebook', 
			'valor' => '3000', 
			'mes' => 1, 
			'ano' => 2021, 
			'tipo' => 'receber'
	]
	];

	public function start(){
		$this->load->library('conta');
		foreach($this->contas as $conta){
			$this->conta->cria($conta);
		}
	}

	public function clear(){
		$this->load->library('conta');
		$this->db->truncate('conta');
	}
}