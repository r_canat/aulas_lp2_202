<?php

class First extends TestCase {

	public function setUp(): void {
		$this->resetInstance();
	}

	function xtestContaDeveInserirRegistroCorretamente(){
		//cen�rio
		$this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
		$this->CI->builder->clear();
		$this->CI->builder->start();

		//a��o
		$this->CI->load->library('conta');
		$res = $this->CI->conta->lista('pagar', 1, 2021);

		//verificacao
		$this->assertEquals('Magalu', $res[0]['parceiro']);
		$this->assertEquals(2021, $res[0]['ano']);
		$this->assertEquals(1, $res[0]['mes']);
		$this->assertEquals(3, sizeof($res));

		$this->assertEquals('Casas Bahia', $res[1]['parceiro']);
		$this->assertEquals(2021, $res[1]['ano']);
		$this->assertEquals(1, $res[1]['mes']);

		$this->assertEquals('Americanas', $res[2]['parceiro']);
		$this->assertEquals(100, $res[2]['valor']);
		$this->assertEquals(2021, $res[2]['ano']);
		$this->assertEquals(1, $res[2]['mes']);

	}

	function xtestContaDeveInformarTotalDeContasAPagarEAReceber(){
	//cen�rio
	$this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
	$this->CI->builder->clear();
	$this->CI->builder->start();

	//a��o
	$this->CI->load->library('conta');
	$res = $this->CI->conta->total('pagar', 1 , 2021);

	//verifica��o
	$this->assertEquals(3600, $res);
	}

	function testContaDeveCalcularSaldoMensal(){
	//cen�rio
	$this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
	$this->CI->builder->clear();
	$this->CI->builder->start();

	//a��o
	$this->CI->load->library('conta');
	$res = $this->CI->conta->saldo(1, 2021);

	//verifica��o
	$this->assertEquals(100, $res);
	}
}